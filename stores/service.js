'use strict';

var Promise = require('bluebird');
var request = Promise.promisify(require('request'));
var find = require('lodash/collection/find');

function Store(uuid) {
  this.options = {
    url: 'http://0.0.0.0:9292/', // could be loaded from a config file
    headers: {authorization: uuid}
  }
}

Store.prototype.get = function (type) {
  return request({
    url: this.options.url + type,
    headers: this.options.headers,
    method: 'GET',
    json: true
  });
};

Store.prototype.save = function (data) {
  return request({
    url: this.options.url + data.type,
    headers: this.options.headers,
    method: 'POST',
    json: true,
    body: data.payload
  });
}

Store.prototype.find = function (data) {
  return this.get(data.type);
};

Store.prototype.findById = function (data) {
  return new Promise(function (resolve, reject) {
    this.get(data.type)
      .then(function (resp) {
        var response = resp[0];
        var body = resp[1];
        resolve({
          body: find(body, {id: data.id}),
          code: response.statusCode
        });
      })
      .error(function (err) {
        return reject(err);
      });
  }.bind(this));
};


module.exports = Store;

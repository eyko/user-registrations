'use strict';

var UsersController = require('./controllers/users');
var uuid = require('node-uuid');
var controller = new UsersController();

var routes = [
  {
    method: 'GET',
    path: '/users',
    handler: function (request, reply) {
      controller.findAll()
        .then(function (data) {
          var response = data[0];
          var body = data[1];
          return reply(body || []).code(response.statusCode);
        })
        .error(function (err) {
        });
    }
  },

  {
    method: 'POST',
    path: '/users',
    handler: function (request, reply) {
      var payload = request.payload;
      // not handling proper http error responses
      // e.g. 400 bad request
      //      409 conflict
      //      422 entity already exists
      //      ... etc
      // the idea is that all requests will get a meaningful
      // response with enough info in the reply payload.
      controller.save(payload)
        .then(function (data) {
          var response = data[0];
          var body = data[1];
          return reply(body).code(response.statusCode);
        })
        .error(function (err) {
          reply('There was an error').code(400);
        });
    }
  },

  {
    method: 'GET',
    path: '/users/{id}',
    handler: function (request, reply) {
      controller.find(request.params.id)
        .then(function (data) {
          if (!data.body) {
            reply().code(404);
          } else {
            reply(data.body).code(data.code);
          }
        })
        .error(function () {
          reply('Error').code(400);
        });
    }
  },

  {
    method: 'POST',
    path: '/users/{id}/connections',
    handler: function (request, reply) {
      controller.connect({users: [request.params.id, request.payload.id]})
        .then(function () {
          reply().code(204);
        })
        .error(function () {
          reply('Error').code(400);
        });
    }
  },

  {
    method: 'GET',
    path: '/users/{id}/connections',
    handler: function (request, reply) {
      controller.getConnections(request.params.id)
        .then(function (data) {
          reply(data);
        })
        .error(function () {
          reply('Error').code(400);
        });
    }
  }

];

module.exports = routes;

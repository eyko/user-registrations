'use strict';

var Hapi = require('hapi');
var routes = require('./routes');
var server = new Hapi.Server();

server.connection({
  host: 'localhost',
  port: 8080
});

server.route(routes);
server.start();

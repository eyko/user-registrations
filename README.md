## User registrations

Basically, one can add a user:

  POST /users  {id:<some-id>, name:<some-name>}

View all users:

  GET /users

View a user

  GET /users/{id}

View a user's connections:

  GET /users/{id}/connections

Create a connection

  POST /users/{id}/connections  {id:<some-other-user-id>}

TODO: getting a user's connections does not get the user objects, but just their ids.


## Installation

    npm install
    node index.js

Note that you will need to have the persistence service running on 0.0.0.0:9292 for it to connect (I hardcoded it).

'use strict';

var uuid = require('node-uuid');
var Store = require('../stores/service');
var Promise = require('bluebird');
var _ = require('lodash');

function UsersController() {
  this.store = new Store(uuid.v4());
};

UsersController.prototype.findAll = function () {
  return this.store.find({type: 'users'});
};

UsersController.prototype.find = function (id) {
  return this.store.findById({type: 'users', id: id});
};

UsersController.prototype.save = function (user) {
  return this.store.save({type: 'users', payload: user});
};

UsersController.prototype.connect = function (data) {
  var users = data.users;
  // not checking that users is an array but would probably
  // check that it's an array and lenght === 2. Anyways:
  return this.store.save({type: 'connections', payload: users});
};

UsersController.prototype.getConnections = function (id) {
  return new Promise(function (resolve, reject) {
    this.store.find({type: 'connections'})
      .then(function (data) {
        var results = data[1];
        // my persistence service is quite dumb and does not
        // know how to filter. I'm not saving connections as lists
        // which perhaps I should've done, so that:
        //   (key:<userId> => value:[<userId1>, <userId2>... <userIdn>])
        // but I didn't want to implement data-types (lists, sets, hashes)
        // in such a simple memory store.
        // therefore, i have to filter here:
        var connections = _(results)
          .filter(function (result) { return _.contains(result, id); })
          .flatten()
          .unique()
          .without(id)
          .values();
        resolve(connections);
      })
      .error(function (err) {
        reject(err);
      });
  }.bind(this));
};

module.exports = UsersController;
